package db

import (
	"database/sql"

	_ "github.com/lib/pq"
)

type Database struct {
	db *sql.DB
}

func NewDatabase() (*Database, error) {
	db, err := sql.Open("postgres", "postgres://chatapppg_user:GwMQUzyvWcH6SQYVOdgXoUfZS6ZMMHud@dpg-ci2ov93hp8u1a1bfsc2g-a.singapore-postgres.render.com/chatapppg")
	if err != nil {
		return nil, err
	}

	return &Database{db: db}, nil
}

func (d *Database) Close() {
	d.db.Close()
}

func (d *Database) GetDB() *sql.DB {
	return d.db
}
