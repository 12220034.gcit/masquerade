import { useState, useEffect, useContext, SyntheticEvent } from 'react';
import { API_URL } from '../../constants';
import { AuthContext } from '../../modules/auth_provider';
import { useRouter } from 'next/router';

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

const Profile = (): JSX.Element => {
  const { user, logout } = useContext(AuthContext);
  const [username, setUsername] = useState(user.username);
  const [isUsernameValid, setIsUsernameValid] = useState(true);
  const router = useRouter();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  const validateUsername = (username: string): boolean => {
    const usernameRegex = /^[a-zA-Z0-9]+$/;
    return usernameRegex.test(username);
  };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  const handleUsernameChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const value = e.target.value;
    setUsername(value);
    setIsUsernameValid(validateUsername(value));
  };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  const updateUsername = async (e: SyntheticEvent): Promise<void> => {
    e.preventDefault();

    if (!isUsernameValid) {
      return;
    }

    try {
      const res = await fetch(`${API_URL}/updateUser`, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include',
        body: JSON.stringify({ id: user.id, username }),
      });

      if (res.ok) {
        // Update the username in the context and navigate back to the profile page
        user.username = username;
        router.push('/profile');
      }
    } catch (err) {
      console.log(err);
    }
  };
  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  const handleLogout = () :void => {
    const confirm = window.confirm("Are you sure you want to delete this?")
    if (confirm){
      logout();
      router.push('/login');
    window.location.reload();
    }
   
    
  };

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  const deleteAccount = (): void => {
    const confirmed = window.confirm("Are you sure you want to delete your account?");
  
    if (confirmed) {
      // User confirmed the account deletion
      try {
        fetch(`${API_URL}/deleteUser`, {
          method: 'DELETE',
          headers: { 'Content-Type': 'application/json' },
          credentials: 'include',
          body: JSON.stringify({ id: user.id }),
        })
          .then((res) => {
            if (res.ok) {
              handleLogout();
              router.push('/login');
            } else {
              console.log('Failed to delete account');
            }
          })
          .catch((err) => {
            console.log(err);
          });
      } catch (err) {
        console.log(err);
        handleLogout();
        router.push('/login');
      }
    }
  };
  
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

  return (
    <div className='flex items-center justify-center min-w-full min-h-screen'>
      <form className='flex flex-col md:w-1/5'>
        <div className='text-3xl font-bold text-center'>
          <span className='text-blue'>Profile</span>
        </div>
        <div className='text-center mt-4 text-gray'>Username: {user.username}</div>
        <div className='text-center mt-2 text-gray'>UID: {user.id}</div>
        <div className='mt-4'>
          {!isUsernameValid && (
            <div className='text-red'>Username can only consist of alphabets and numbers</div>
          )}
          <input
            placeholder='username'
            className={`p-3 mt-2 rounded-md border-2 w-full ${
              isUsernameValid ? 'border-grey' : 'border-red'
            } focus:outline-none ${isUsernameValid ? 'focus:border-blue' : ''}`}
            value={username}
            onChange={handleUsernameChange}
          />
        </div>
        <button
          className='p-3 mt-6 rounded-md bg-blue font-bold text-white'
          type='submit'
          onClick={updateUsername}
        >
          Update Username
        </button>
        <button className='p-3 mt-4 rounded-md bg-red text-white' onClick={() => { deleteAccount(); handleLogout(); }}>
          Delete Account
        </button>
      </form>
    </div>
  );
};

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

export default Profile;
