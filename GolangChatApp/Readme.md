
#  ♫.•°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫
#  ───▄▄▄▄▄▄
#  ─▄▀░░░░░░▀▄░██░██ █████ ██░░ ██░░ █████
#  ▐░▄▄▄░░▐▀▌░▌██▄██ ██▄▄▄ ██░░ ██░░ ██░██
#  ▐░░░░░░░░░░▌██▀██ ██▀▀▀ ██░░ ██░░ ██░██
#  ▐░░▀▄░░▄▀░░▌██░██ █████ ████ ████ █████
#  ─▀▄░░▀▀░░▄▀
#  ───▀▀▀▀▀▀--- and welcome
#  ♫.•°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫°”˜˜”°•.♫ 


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->


<!-- In order to run this webapp it is important to first setup the database. Now for Local Host (which is what this is automatically configured to be) all you need to do is set up the backend in docker by following the steps below. Simply copy and paste the following commands into the database. All the commands need to executed in the terminal.-->

<!-- the following commands need to be executed in the server directory -->
cd server

docker pull postgres:15-alpine
<!-- this command pulls the image for creating the database -->

docker run --name postgres15 -p 5433:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=password -d postgres:15-alpine
<!-- this command is used to create and run a Docker container based on the image we pulled earlier -->

docker exec -it postgres15 createdb --username=root --owner=root go-chat
<!-- this command creates the postgresql database named "go-chat" inside the container that we ran earlier -->

<!-- 
With this your database has been made!!
Congrats!!
Now we need to setup your table
-->

<!-- 
You can make the table manually however I made use of golang-migrate, to create my table. 
For this you're going to need to install migrate. Go to this link below to learn how to install migrate on your computer (or to just read the documentations) 
-->

<!-- Then you can execute the following command to create a new migration -->
migrate creat -ext sql -dir db/migrations add_users_table

<!-- for now you can just use this command to simply connect or even update the database -->
migrate -path db/migrations -database "postgresql://root:password@localhost:5433/go-chat?sslmode=disable" -verbose up

<!-- and this command is for going back to a previous save -->
migrate -path db/migrations -database "postgresql://root:password@localhost:5433/go-chat?sslmode=disable" -verbose down

<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

<!-- Now you can do these 2 things
First open 2 seperate terminals and  -->
cd server 
cd client 

<!-- install all the required dependencies, run  -->
go mod tidy 
npm install 

<!-- if any package especially in the front end refuses to install on its own then you can install them manually. I encountered these problems with toastify but they were an easy fix. After all that is done you can now run these 2 commands -->
go run cmd/main.go
npm run dev

with this your webapp should be up and running.
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

<!-- Now lets talk about navigating the database. Open docker terminal and use this commands to navigate through it -->

psql -U root -d go-chat 
<!-- this is for logging into postgres in docker. root is our username and go-chat is the name of our database -->

\dt 
<!-- this is to list the tables -->

SELECT * FROM users; 
<!-- this is to show all from a table -->

DELETE FROM users; 
<!-- this is to delete all from a table -->

\q
<!-- this is to quit the terminal from docker -->


<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->
<!--------------------------------- Well Thats About it. If you have any queries please feel free to message me. --------------------------------->

# ────────────¸,o​¤°“°¤o,¸
# ─────────── (….◕​ ‿ ◕.…)
# ───────── oOO——`​♥´——OOo
#  ▀█▀─█▄█─█▀█─█▄─█─█▄▀──█▄█─█▀█─█─█
#  ─█──█▀█─█▀█─█─▀█─█▀▄───█──█▄█─█▄█
#  ───── for your
#  ─────────── time
 